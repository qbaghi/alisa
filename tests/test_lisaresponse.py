

if __name__ == '__main__':

    # import alisa
    from alisa import lisaresponse, gwsources, instrument, lisaorbit, lisatdi
    import numpy as np
    from matplotlib import pyplot as plt
    import loadsourceconfig

    # Retrieve source configuration
    # ==========================================================================
    conf_file_path = '/Users/qbaghi/Codes/python/gwaves/configs/'
    conf_file_name = 'GBsourceconfig_1year_A=2e-20_monof0=2e-4.txt'
    # Change the parameters according to the config file
    conf = loadsourceconfig.loadconfig(conf_file_path + conf_file_name)
    # Get source parameters
    param_dic = gwsources.ldc2dic(conf.p)
    # Source class
    gwsrc = gwsources.UCBSource(param_dic['f_0'],
                                param_dic['f_dot'],
                                param_dic['A'],
                                param_dic['phi_0'],
                                param_dic['theta'],
                                param_dic['phi'],
                                param_dic['psi'],
                                param_dic['incl'],
                                name='UCB')
    ts = conf.p.get("Cadence")
    tobs = conf.p.get("ObservationDuration")
    instr = instrument.LISAInstrument(tobs=tobs, fs=1/ts)
    # Definition of LISA orbit
    orb = lisaorbit.LISAorbit(phi_rot=0)
    # Definition of TDI algorithm
    tdint = lisatdi.LIDATdi(generation='1st')
    # Instanciation of LISA response
    lisaresp = lisaresponse.LISAResponse(instr, orb, tdint)

    # Phasemeter measurements
    print("Building symbolic functions...")
    lisaresp.build_gw_signal_by_spacecraft(gwsrc, ffreq=True)
    # Time vector
    print("Forming time vector...")
    t_val = instr.compute_time_vector()
    # Computation
    print("Computation of arm response at requested times...")
    pm, pm_prime = lisaresp.compute_arm_response(t_val)

    # Simulate again the signal in single-links to be sure
    # ==========================================================================
    print("Loading data...")
    pm_sig = np.load('../data/pm.npy')
    pm_prime_sig = np.load('../data/pm_prime.npy')
    print("Data loaded.")

    print("Mean error:" + str(np.mean(np.abs(pm_sig[:, 0] - pm[:, 0]))))

    plt.figure(1)
    plt.plot(t_val, pm_sig[:, 0], 'k', label='Saved reference.')
    plt.plot(t_val, pm[:, 0], 'r--', label='Current simulation')
    plt.legend(loc='upper left')
    plt.show()
