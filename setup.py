#!/usr/bin/env python

from setuptools import setup, find_packages
# from distutils.core import setup

setup(name='alisa',
      version='1.0',
      description='A simple analytical simulator for LISA space mission in the time domain.',
      author='Quentin Baghi',
      author_email='quentin.baghi@protonmail.com',
      url='https://gitlab.com/qbaghi/alisa',
      packages=find_packages(),
      )
