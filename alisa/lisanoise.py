import numpy as np
from alisa.utils import LagrangeInterpolator
from alisa.utils import fir_lowpass_filter
from pyfftw.interfaces.numpy_fft import fft, ifft


def generate_freq_noise_from_psd(psd, fe, myseed=None):
    """
    Generate noise in the frequency domain from the values of the psd.

    Function generating a colored noise from a vector containing the psd.
    The psd contains Np points such that Np > 2N and the output noise should
    only contain N points in order to avoid boundary effects. However, the
    output is a 2N vector containing all the generated data. The troncature
    should be done afterwards.

    References : Timmer & König, "On generating power law noise", 1995

    Parameters
    ----------
    psd : array_like
        vector of size n_dsp continaing the noise psd calculated at frequencies
        between -fe/n_dsp and fe/n_dsp where fe is the sampling frequency and N
        is the size of the time series (it will be the size of the returned
        temporal noise vector b)
    N : scalar integer
        Size of the output time series
    fe : scalar float
        sampling frequency
    myseed : scalar integer or None
        seed of the random number generator

    Returns
    -------
        bf : numpy array
        frequency sample of the colored noise (size N)
    """

    # Size of the psd
    n_dsp = len(psd)
    # Initialize seed for generating random numbers
    np.random.seed(myseed)

    n_fft = np.int((n_dsp-1)/2)
    # Real part of the Noise fft : it is a gaussian random variable
    noise_tf_real = np.sqrt(0.5) * psd[0:n_fft + 1] * np.random.normal(
        loc=0.0, scale=1.0, size=n_fft + 1)
    # Imaginary part of the Noise fft :
    noise_tf_im = np.sqrt(0.5) * psd[0:n_fft + 1] * np.random.normal(
        loc=0.0,  scale=1.0, size=n_fft + 1)

    # The Fourier transform must be real in f = 0
    noise_tf_im[0] = 0.
    noise_tf_real[0] = noise_tf_real[0]*np.sqrt(2.)

    # Create the NoiseTF complex numbers for positive frequencies
    noise_tf = noise_tf_real + 1j*noise_tf_im

    # To get a real valued signal we must have NoiseTF(-f) = NoiseTF*
    if n_dsp % 2 == 0:
        # The TF at Nyquist frequency must be real in the case of an even
        # number of data
        noise_sym0 = np.array([psd[n_fft + 1] * np.random.normal(0, 1)])
        # Add the symmetric part corresponding to negative frequencies
        noise_tf = np.hstack((noise_tf, noise_sym0,
                              np.conj(noise_tf[1:n_fft+1])[::-1]))

    else:

        # noise_tf = np.hstack( (noise_tf, Noise_sym[::-1]) )
        noise_tf = np.hstack((noise_tf, np.conj(noise_tf[1:n_fft+1])[::-1]))

    return np.sqrt(n_dsp*fe/2.) * noise_tf


def generate_noise_from_psd(psd, fe, myseed=None) :
    """
    Function generating a colored noise from a vector containing the psd.
    The psd contains Np points such that Np > 2N and the output noise should
    only contain N points in order to avoid boundary effects. However, the
    output is a 2N vector containing all the generated data. The troncature
    should be done afterwards.

    References : Timmer & König, "On generating power law noise", 1995

    Parameters
    ----------
    psd : array_like
        vector of size N_DSP continaing the noise psd calculated at frequencies
        between -fe/N_DSP and fe/N_DSP where fe is the sampling frequency and N
        is the size of the time series (it will be the size of the returned
        temporal noise vector b)
    N : scalar integer
        Size of the output time series
    fe : scalar float
        sampling frequency
    myseed : scalar integer or None
        seed of the random number generator

    Returns
    -------
        b : numpy array
        time sample of the colored noise (size N)
    """

    return ifft(generate_freq_noise_from_psd(psd, fe, myseed=myseed))


class NoiseModel(object):

    def __init__(self, fs, fs_simu=None):
        """
        Class defining LISA's noise models

        Parameters
        ----------
        fs : scalar float
            output sampling frequency
        fs_simu : scalar integer
            simulation sampling frequency

        Returns
        -------
        None.
            Instantiate the class.

        """

        # Output sampling frequency
        self.fs = fs
        # Noise simulation internal sampling frequency
        if fs_simu is None:
            self.fs_simu = fs * 10
        else:
            self.fs_simu = fs_simu
        # Laser absolute frequency stability [Hz/sqrt(Hz)]
        self.a0 = 28.2
        # Laser central frequency
        self.nu0 = 299792458.0/1064e-9
        # TM displacement noise amplitude spectral density [m]
        self.atm = 3e-15
        # OMS constant amplitude spectral density in displacement [m]
        self.aoms = 15e-12
        # Acceleration knee frequency
        self.fa1 = 4e-4
        # Acceleration positive slope frequency
        self.fa2 = 8e-3
        # OMS noise knee frequency
        self.fa3 = 2e-3
        # Low-frequency cut-off to avoid outflows
        self.f_low_cut = 1e-6

    def freq_noise_model(self, f):
        """

        PSD of laser frequency noise converted in fractional frequency
        in 1/Hz

        Parameters
        ----------
        f : array_like
            frequency in Hz

        Returns
        -------
        s : array_like
            laser frequency noise PSD in fractional frequency (1/Hz)

        Reference
        ---------
        [1] O. Jennrich, LISA technology and instrumentation, 2009
        https://arxiv.org/pdf/0906.2901.pdf
        Cf. also LISANode:
        [2] LISA Performance Model and Error Budget. LISA-LCST-INST-TN-003
        (Jun. 2018).
        [3] Thompson, R., et al., A flight-like optical reference cavity for
        GRACE follow-on laser frequency stabilization. 2011, Joint Conference
        on the IEEE International.

        """

        s_freq = self.a0**2

        return s_freq*(1/self.nu0)**2 * np.ones(len(f))

    def other_noise_model(self, f, c=299792458.0):
        """

        PSD of other measurement noises converted in fractional frequency
        in 1/Hz

        [1] LISA Performance Model and Error Budget. LISA-LCST-INST-TN-003
        (Jun. 2018).

        """
        # TM acceleration noise [(m/s2)^2/Hz]
        sa = self.atm ** 2 * (1 + (self.fa1 / (f + self.f_low_cut))**2)
        sa *= (1 + (f/self.fa2)**4)
        # Convert into relative frequency
        sa /= (2 * np.pi * (f + self.f_low_cut) * c) ** 2
        # Optical metrology system noise
        so = (self.aoms)**2 * (1 + (self.fa3/(f + self.f_low_cut))**4)
        # Convert in relative frequency units
        so *= (2.0 * np.pi * f / c)**2

        return sa + so

    def generate_laser_freq_noise(self, n_fft, fs, dtype=np.float128):
        """
        Generate laser frequency noises directly in the time domain

        """

        # n_fft = 2**LE.nextpow2(N)
        f = np.fft.fftfreq(n_fft)*fs
        f[0] = f[1]
        psd = self.freq_noise_model(np.abs(f))
        # PSD = freq_noise_model(f[f>0])
        # PSD = noise.symmetrize(PSD,n_fft)

        p_matrix = np.array([np.real(generate_noise_from_psd(
            np.sqrt(psd), fs)) for j in range(3)]).T

        return p_matrix

    def generate_other_noises(self, n_fft, fs, dtype=np.float128):
        """

        :param n_fft:
        :param fs:
        :param cut: scalar float
            Ratio between the low-pass filter cutting frequency and the Nyquist
            frequency
        :param dtype:
        :param apply_filter:
        :return:
        """

        # n_fft = 2**LE.nextpow2(N)
        f = np.fft.fftfreq(n_fft)*fs
        f[0] = f[1]

        # S_IM = inertial_mass_noise_model(np.abs(f))
        # S_SN = shot_noise_model(np.abs(f))
        # S_OOP = OOP_noise_model(np.abs(f))

        # PSD = S_IM + S_SN + S_OOP
        psd = self.other_noise_model(np.abs(f))
        n_matrix = np.array([np.real(generate_noise_from_psd(
            np.sqrt(psd), fs)) for j in range(6)]).T

        return n_matrix

    def generate_phasemeter_noises(self, n_data, delay_func, fcut=0.1,
                                   order=5, nint=31,
                                   noise_fact=None, method='time'):
        """
        Generate the noises of each phasemeter measurement according to the
        equations

        s_{i}(t) = p_{i'+1}(t-L_{i+2}/c) + p_{i}(t)
        s_{i'}(t) = p_{i+2}(t-L_{i'+1}/c) + p_{i'}(t)

        Parameters
        ----------
        n_data : scalar integer
            size of the time series to generate
        delay_func : array_like
            callable function giving vectors containing the delays L_i/c as a
            function of time.
        fcut : float
            cutting frequency of the anti-aliasing filters
        nint : int
            order of the interpolating Lagrange filters
        sagnac : boolean
            if True, assume a rotating constellation with Sagnac effect
        noise_fact : bytearray
            vector of scaling factor to apply to the non-laser noises to get
            different levels.
        method : str
            noise generation method among {'time', 'frequency'}.
            Default is 'time': uses Lagrange fractional filters in the
            time domain to delay the noise time series.
            If 'frequency' is chosen, the delays are directly applied in the
            frequency domain.

        Returns
        -------
        pm_matrix : numpy array
            matrix of size N x 3 containing the 3 time series of the phasemeter
            noises s_{i}
        pm_prime_matrix : numpy array
            matrix of size N x 3 containing the 3 time series of the phasemeter
            noises s_{i'}

        """

        n_simu = np.int(n_data * self.fs_simu / self.fs)
        # Number of points for the noise generation
        # q = int(np.log(2 * n_simu)/np.log(2.)) + 1
        # n_fft = 2**q
        n_fft = 2 * n_simu
        t_fft = np.arange(0, n_fft) / self.fs_simu
        print("Number of generated random frequency points will be "
              + str(n_fft))
        # Generate the noises in the time domain
        p_matrix = self.generate_laser_freq_noise(n_fft, self.fs_simu,
                                                  dtype=np.float128)
        # Assume same laser for reception and emission
        n_matrix = self.generate_other_noises(n_fft, self.fs_simu,
                                              dtype=np.float128)

        # Apply anti-aliasing filtering:
        p_matrix = np.array([fir_lowpass_filter(p_matrix[:, i],
                                                fcut, self.fs_simu,
                                                order=order,
                                                beta=8.6)
                             for i in range(p_matrix.shape[1])],
                            dtype=p_matrix.dtype).T

        n_matrix = np.array([fir_lowpass_filter(n_matrix[:, i],
                                                fcut, self.fs_simu,
                                                order=order,
                                                beta=8.6)
                             for i in range(n_matrix.shape[1])],
                            dtype=p_matrix.dtype).T

        # Apply a factor to the other noises if necessary
        if noise_fact is not None:
            n_matrix = np.sqrt(noise_fact) * n_matrix
        # Calculate delays at all the times of the simulated noise
        delays = np.array([delay_func(t_fft, i+1) for i in range(3)],
                          dtype=np.float128).T
        delays_prime = np.array([delay_func(t_fft, -i-1) for i in range(3)],
                                dtype=np.float128).T
        # Calculate the new time vector at which we should interpolate the
        # noise
        t_int = np.array([t_fft - delays[:, i] for i in range(3)],
                         dtype=np.float128).T
        # The required delayed time may be negative
        min_time = np.min([np.min(t_int[0, i])
                           for i in range(delays.shape[1])])
        # We need noise data from time t - min(d(t)) - nint/2 where nint is the
        # order of the lagrange interpolators
        i0 = np.int(np.around(np.abs(min_time)*self.fs_simu))
        i0 += np.int((nint - 1) / 2)+1

        # Therefore we have to shift the time at wich the output data will be
        # generated
        tinds = np.arange(i0, i0 + n_simu)
        t_out = tinds / self.fs_simu
        # Times of the measured data that we need
        t_simu = t_fft[0:n_simu + 2*i0]

        # We need to interpolate the noise at times t - delays. We should do
        # that statistically. In LISANode and LISACode, they use Lagrange
        # interpolation, which is not realistic as it does not preserve the
        # noise statistics. Let's do like them for now.
        print("starting interpolation...")
        # Instanciate Lagrange interpolator classes, we give all the data from
        # t=0
        li = [LagrangeInterpolator(t_simu, p_matrix[0:n_simu + 2*i0, i],
                                   nint)
              for i in range(3)]
        # noise sequence indices for pm
        n_inds = [1, 2, 0]
        # delay sequence indices for pm
        d_inds = [2, 0, 1]
        # noise sequence indices for pm_prime
        n_inds_p = [2, 0, 1]
        # delay sequence indices for pm_prime
        d_inds_p = [1, 2, 0]

        if method == 'time':

            # Initialization of phasemeter matrices
            pm = np.empty((n_simu, 3), dtype=np.float128)
            pm_prime = np.empty((n_simu, 3), dtype=np.float128)

            for i in range(3):
                # Arrangement primary SC i
                pm[:, i] = - p_matrix[tinds, i]
                pm[:, i] += li[n_inds[i]].interp(t_out-delays[0:n_simu, d_inds[i]])
                pm[:, i] += n_matrix[tinds, i]
                print("Interpolation..." + str(np.int(100 * (2*(i+1)-1)/6)) + "%")
                # Arrangement secondary SC i'
                pm_prime[:, i] = - p_matrix[tinds, i]
                pm_prime[:, i] += li[n_inds_p[i]].interp(
                    t_out - delays_prime[0:n_simu, d_inds_p[i]])
                pm_prime[:, i] += n_matrix[tinds, i+3]
                print("Interpolation..." + str(np.int(100 * (2 * (i + 1)) / 6))
                      + "%")

        elif method == 'frequency':

            # Initialization of phasemeter matrices
            pm_fft = np.empty(p_matrix.shape, dtype=np.complex256)
            pm_prime_fft = np.empty(p_matrix.shape, dtype=np.complex256)
            # ! Only valid for constant delays
            p_matrix_fft = fft(p_matrix, axis=0)
            n_matrix_fft = fft(n_matrix, axis=0)
            f = np.fft.fftfreq(n_fft) * self.fs_simu

            # Apply all the delays in the Fourier domain
            for i in range(3):
                # Arrangement primary SC i
                pm_fft[:, i] = - p_matrix_fft[:, i]
                phasor = np.exp(-2*np.pi*1j * delays[0, d_inds[i]] * f)
                pm_fft[:, i] += p_matrix_fft[:, n_inds[i]] * phasor
                pm_fft[:, i] += n_matrix_fft[:, i]
                # Arrangement primary SC i'
                pm_prime_fft[:, i] = - p_matrix_fft[:, i]
                phasor = np.exp(-2*np.pi*1j * delays_prime[0, d_inds_p[i]] * f)
                pm_prime_fft[:, i] += p_matrix_fft[:, n_inds_p[i]] * phasor
                pm_prime_fft[:, i] += n_matrix_fft[:, i+3]

            # Convert back to time domain
            pm = np.real(ifft(pm_fft, axis=0))[tinds, :]
            pm_prime = np.real(ifft(pm_prime_fft, axis=0))[tinds, :]

        print("interpolation complete.")

        # Decimation and anti-aliazing filter if necessary
        pm_out = np.empty((n_data, 3), dtype=np.float128)
        pm_prime_out = np.empty((n_data, 3), dtype=np.float128)
        n_matrix_out = np.empty((n_data, 6), dtype=np.float128)
        delays_out = np.empty((n_data, 3), dtype=np.float128)
        delays_prime_out = np.empty((n_data, 3), dtype=np.float128)

        for j in range(pm.shape[1]):
            # Down-sampling factor
            m = np.int(self.fs_simu/self.fs)
            # Low-pass filter the data
            pm_out[:, j] = fir_lowpass_filter(pm[:, j],
                                              fcut,
                                              self.fs_simu,
                                              order=order,
                                              beta=8.6)[0::m]
            pm_prime_out[:, j] = fir_lowpass_filter(pm_prime[:, j],
                                                    fcut,
                                                    self.fs_simu,
                                                    order=order,
                                                    beta=8.6)[0::m]
            n_matrix_out[:, j] = fir_lowpass_filter(n_matrix[i0:i0+n_simu, j],
                                                    fcut,
                                                    self.fs_simu,
                                                    order=order,
                                                    beta=8.6)[0::m]
            n_matrix_out[:, j+3] = fir_lowpass_filter(n_matrix[i0:i0+n_simu,
                                                               j+3],
                                                      fcut,
                                                      self.fs_simu,
                                                      order=order,
                                                      beta=8.6)[0::m]
            # # Try without filtering
            # pm_out[:, j] = pm[0::m, j]
            # pm_prime_out[:, j] = pm_prime[0::m, j]
            # n_matrix_out[:, j] = n_matrix[i0:i0+n_simu:m, j]
            # n_matrix_out[:, j+3] = n_matrix[i0:i0+n_simu:m, j+3]
            # Delays sampled at output cadence
            delays_out[:, j] = delays[0:n_simu, j][0::m]
            delays_prime_out[:, j] = delays_prime[0:n_simu, j][0::m]

        return pm_out, pm_prime_out, n_matrix_out, delays_out, delays_prime_out
