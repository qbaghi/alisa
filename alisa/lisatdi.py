# -*- coding: utf-8 -*-
# Author: Quentin Baghi 2018
import numpy as np
import copy


class LIDATdi(object):

    def __init__(self, generation='1st'):

        self.generation = generation
        self.c = 299792458.0

    # def apply_single_shift(self, sfunc, arm_length):
    #     """
    #
    #     Parameters
    #     ----------
    #     sfunc : function
    #         function giving the signal to be shifted as a function of time, where time is a numpy array
    #     arm_length : function
    #         arm length given as a python function of time, where time is a numpy array
    #
    #     Returns
    #     -------
    #     sfunc_shifted : callable function
    #         shifted signal function
    #
    #     """
    #
    #     return lambda t: sfunc(t - arm_length(t)/self.c)

    def apply_single_shift(self, t, sfunc, arm_length):
        """

        Parameters
        ----------
        t : sympy.Symbol instance
            variable used to represent time
        sfunc : sympy.core.add instance
            signal to be shifted, given as a symbolic function parametrized by a
            variable t representing time
        arm_length : sympy.core.add instance
            arm length, given as a symbolic function parametrized by a variable t
            representing time

        Returns
        -------
        sfunc_shifted : sympy.core.add instance
            shifted signal function
        """

        return sfunc.subs(t, t - arm_length/self.c)

    def apply_shifts(self, t, sfunc, applylist, L, L_prime):
        """

        Parameters
        ----------
        t : sympy.Symbol instance
            variable used to represent time
        sfunc : sympy.core.add instance
            signal to be shifted, given as a symbolic function parametrized by a
            variable t representing time
        applylist : list of relative integers
            list that contains the indices of the shits in the order to be applied.
            Positive integers (i=1,2,3) represent the arm numbers (i=1,2,3) in the
            direction from S/C i-1 to S/C i+1 and negative integers represent
            (i=-1,-2,-3) represent the arm numbers (i=1',2',3') in the direction
            from S/C i+1 to S/C i-1
        L : list of functions
            list where L[i-1] is a function giving the arm length L_si as a function
            of time t
        L_prime : list of functions
            list where L[i-1] is a function giving the arm length L_i' as a function
            of time t

        Returns
        -------
        sfunc_shifted : callable function
            shifted signal function

        """

        sfunc_shifted = copy.deepcopy(sfunc)

        for index in applylist:

            if index > 0:

                sfunc_shifted = self.apply_single_shift(t, sfunc_shifted,
                                                        L[index-1])
            else:
                sfunc_shifted = self.apply_single_shift(t, sfunc_shifted,
                                                        L_prime[np.abs(index)-1])

        return sfunc_shifted

    def build_tdi_response(self, t, s_GW, s_GW_prime, L, L_prime):
        """
        This functions takes as input all the symbolic functions describing the
        arm responses to GW on spacecrafts i=1,2,3.

        Parameters
        ----------
        s_GW : list of numpy arrays
            s_GW[i-1] (python indices begins from 0) gives the signal measured
            in spacecraft i travelling along the arm L_{i+2} measured by
            optical bench 1, as a function of a parameter dictionary params
        s_GW_prime : list of numpy arrays
            s_GW_prime[i-1] gives the signal measured in spacecraft i
            travelling along the arm L_{i+1} measured by adjascent optical
            bench 1', as a function of a parameter dictionary params
        L : list of python functions
            L[i] is the arm length as a function of time t in the clockwise
            direction (from S/C i-1 to S/C i+1) -> TO BE CHECKED
        L_prime : list of python functions
            L_prime[i] is the arm length as a function of time t in the
            anticlockwise direction (from S/C i+1 to S/C i-1) -> TO BE CHECKED
        generation : string {'1st','2nd'}
            indicate the TDI generation to apply


        Returns
        -------
        x_func : sympy.core.add instance
            symbolic function taking as input the dictionary of source parameters and
            computing the TDI response as a function of time

        """

        if self.generation == '1st':
            # For tdi x
            x_func = self.apply_shifts(t, s_GW_prime[1], [3, 2, -2], L, L_prime)\
            + self.apply_shifts(t, s_GW[0], [2, -2], L, L_prime) \
            + self.apply_shifts(t, s_GW[2], [-2], L, L_prime) \
            + s_GW_prime[0] \
            - self.apply_shifts(t, s_GW[2], [-2, -3, 3], L, L_prime) \
            - self.apply_shifts(t, s_GW_prime[0], [-3, 3], L, L_prime) \
            - self.apply_shifts(t, s_GW_prime[1], [3], L, L_prime) \
            - s_GW[0]

            # For tdi y
            y_func = self.apply_shifts(t, s_GW_prime[2], [1, 3, -3], L, L_prime) \
                     + self.apply_shifts(t, s_GW[1], [3, -3], L, L_prime) \
                     + self.apply_shifts(t, s_GW[0], [-3], L, L_prime) \
                     + s_GW_prime[1] \
                     - self.apply_shifts(t, s_GW[0], [-3, -1, 1], L, L_prime) \
                     - self.apply_shifts(t, s_GW_prime[1], [-1, 1], L, L_prime) \
                     - self.apply_shifts(t, s_GW_prime[2], [1], L, L_prime) \
                     - s_GW[1]

            # For tdi z
            z_func = self.apply_shifts(t, s_GW_prime[0], [2, 1, -1], L, L_prime) \
                     + self.apply_shifts(t, s_GW[2], [1, -1], L, L_prime) \
                     + self.apply_shifts(t, s_GW[1], [-1], L, L_prime) \
                     + s_GW_prime[2] \
                     - self.apply_shifts(t, s_GW[1], [-1, -2, 2], L, L_prime) \
                     - self.apply_shifts(t, s_GW_prime[2], [-2, 2], L, L_prime) \
                     - self.apply_shifts(t, s_GW_prime[0], [2], L, L_prime) \
                     - s_GW[2]

        elif self.generation == '2nd':
            raise NotImplementedError("2nd generation TDI not implemented yet")

        else:
            raise ValueError("Unkown TDI generation")

        return x_func, y_func, z_func

    # def apply_tdi_response(self, s_GW, s_GW_prime, L, L_prime):
    #     """
    #     This functions takes as input all the symbolic functions describing the
    #     arm responses to GW on spacecrafts i=1,2,3.
    #
    #     Parameters
    #     ----------
    #     s_GW : list of python functions
    #         s_GW[i-1] (python indices begins from 0) gives the signal measured in
    #         spacecraft i travelling along the arm L_{i+2} measured by optical bench 1,
    #         as a function of a parameter dictionary params
    #     s_GW_prime : list of python functions
    #         s_GW_prime[i-1] gives the signal measured in spacecraft i travelling along the
    #         arm L_{i+1} measured by adjascent optical bench 1', as a function of a
    #         parameter dictionary params
    #     L : list of python functions
    #         L[i] is the arm length as a function of time t in the clockwise direction
    #         (from S/C i-1 to S/C i+1) -> TO BE CHECKED
    #     L_prime : list of python functions
    #         L_prime[i] is the arm length as a function of time t in the anticlockwise
    #         direction (from S/C i+1 to S/C i-1) -> TO BE CHECKED
    #     generation : string {'1st','2nd'}
    #         indicate the TDI generation to apply
    #
    #
    #     Returns
    #     -------
    #     x_func : callable
    #         function taking as input the dictionary of source parameters and
    #         computing the TDI response as a function of time
    #
    #     """
    #
    #     if self.generation == '1st':
    #
    #         def x_func(t):
    #
    #             x = self.apply_shifts(s_GW_prime[1], [3, 2, -2], L, L_prime)(t) \
    #                 + self.apply_shifts(s_GW[0], [2, -2], L, L_prime)(t)  \
    #                 + self.apply_shifts(s_GW[2], [-2], L, L_prime)(t)  \
    #                 + s_GW_prime[0](t)  \
    #                 - self.apply_shifts(s_GW[2], [-2, -3, 3], L, L_prime)(t)  \
    #                 - self.apply_shifts(s_GW_prime[0], [-3, 3], L, L_prime)(t)  \
    #                 - self.apply_shifts(s_GW_prime[1], [3], L, L_prime)(t)  \
    #                 - s_GW[0](t)
    #
    #             return x
    #
    #     elif self.generation == '2nd':
    #         raise NotImplementedError("2nd generation TDI not implemented yet")
    #
    #     else:
    #         raise ValueError("Unkown TDI generation")
    #
    #     return x_func
