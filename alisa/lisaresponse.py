import numpy as np
# from sympy import *
from sympy import Symbol, sin, cos, lambdify, Matrix, sqrt
# from numpy import sqrt
# from numpy import sin, cos, sqrt


def derivative(f, a, method='central', h=0.01):
    '''Compute the difference formula for f'(a) with step size h.

    Parameters
    ----------
    f : function
        Vectorized function of one variable
    a : number
        Compute derivative at x = a
    method : string
        Difference formula: 'forward', 'backward' or 'central'
    h : number
        Step size in difference formula

    Returns
    -------
    float
        Difference formula:
            central: f(a+h) - f(a-h))/2h
            forward: f(a+h) - f(a))/h
            backward: f(a) - f(a-h))/h
    '''

    if method == 'central':
        return (f(a + h) - f(a - h))/(2*h)
    elif method == 'forward':
        return (f(a + h) - f(a))/h
    elif method == 'backward':
        return (f(a) - f(a - h))/h
    else:
        raise ValueError("Method must be 'central', 'forward' or 'backward'.")


def sympy2func(expr, usedlib="numpy"):
    """
    This function convert any sympy symbolic expression depending on some symbol
    variables V1,V2,...,Vn in a python lambda function of numeric variables
    V1,...,Vn.

    Parameters
    ----------
    expr : sympy.core.add.Add instance
        sympy expression to be converted

    Returns
    -------
    pfunc : function class instance
        numerical function that takes as input a dictionary containing as many
        entries as there are symbolic variable in expr, with keys given by
        the string conversion of each symbolic variable name
    symbvars_string : list of strings
        list of parameters names

    """

    # Get all the symbolic variables contained in the expression
    freesymb = expr.free_symbols
    symbvars = list(freesymb)
    symbvars_string = [str(svar) for svar in symbvars]

    lfunc = lambdify(freesymb, expr, usedlib)

    # Create a function of a dictionary for the parameters
    def pfunc(param_dic):
        params = [param_dic[varname] for varname in symbvars_string]
        return lfunc(*params)

    print("Extracted parameters:")
    print(symbvars_string)

    return pfunc
    # return lfunc


class LISAResponse(object):
    """

    Class defining the geometry of the antenna pattern and methods to compute
    LISA response to GW

    """

    def __init__(self, instrument, orbit, tdi):

        self.instrument = instrument
        self.orbit = orbit
        # Single-link
        self.s_gw_func = None
        self.s_gw_prime_func = None
        self.s_gw_call = None
        self.s_gw_prime_call = None
        # TDI functions
        self.tdi = tdi
        self.tdi_func = None
        self.tdi_call = None

        # Symbolic variables
        self.t = Symbol('t')

    def cyclic_perm(self, i):
        """
        Function realizing cyclic permutations for L_i
        such that i={1,2,3}
        For example if i = 4 it becomes i=1
        and if i=-1 it becomes i=3
        """
        # Implements circular permutations
        if i > 0:
            j = i
            while j > 3:
                j = j - 3

        elif i < 0:
            j = i
            while j < -3:
                j = j + 4

        return np.int(j)

    def build_arm_vect(self, i, deriv=0):
        """
        Function computing the arm vector n_i(t) in the rigid constellation
        approximation
        such that

        n_i(t) = ( x_{i+1} - x_{i+2}) / (||x_{i+1} - x_{i+2}||)

        Parameters
        ----------
        i : scalar integer
            the index of considered arm. if i is negative, then it means that
            it n_i_prime(t) = - n_i(t)
        phi_T : sympy.Symbol instance
            orbital angle locating the constellation (true anomaly)

        Returns
        -------
        n_vect : sympy.matrix instance
            unit vector along arm direction


        """

        phi_i = (self.orbit.phi_rot_i_func(i + 2)
                 + self.orbit.phi_rot_i_func(i + 1)) / 2
        # This is from Phi_rot_i = (i-1)*2*pi/3 - Phi_rot
        # Arm unit vector n_i = frac{ u_{i+1} - u_{1+2} }{ || u_{i+1} - u_{1+2} ||}
        n_vect = 1 / 4. * Matrix(
            [-sin(2 * self.orbit.phi_T - phi_i) - 3 * sin(phi_i),
             cos(2 * self.orbit.phi_T - phi_i) + 3 * cos(phi_i),
             2 * sqrt(3) * sin(self.orbit.phi_T - phi_i)])

        if deriv == 1:
            n_vect = n_vect.diff(self.t)

        return np.sign(i) * n_vect

    # def projection_functions(self, source, n_vect, n_vect_dot, deriv=0):
    #     """
    #     Compute projection functionals chi on the interferometer arm of direction n
    #
    #     """
    #
    #     if deriv == 0:
    #         # Projection functionals
    #         chi_plus = (source.theta_vect.T.dot(n_vect)) ** 2 - (source.phi_vect.T.dot(n_vect)) ** 2
    #         chi_cros = 2 * (source.theta_vect.T.dot(n_vect)) * (source.phi_vect.T.dot(n_vect))
    #
    #     elif deriv == 1:
    #
    #         chi_plus = 2 * (source.theta_vect.T.dot(n_vect_dot)) * (source.theta_vect.T.dot(n_vect)) \
    #                    - 2 * (source.phi_vect.T.dot(n_vect_dot)) * (source.phi_vect.T.dot(n_vect))
    #         chi_cros = 2 * ((source.theta_vect.T.dot(n_vect_dot)) * (source.phi_vect.T.dot(n_vect)) +
    #                         (source.theta_vect.T.dot(n_vect)) * (source.phi_vect.T.dot(n_vect_dot)))
    #
    #     return chi_plus[0], chi_cros[0]

    def build_projection_functions(self, source, n_vect, deriv=False):
        """
        Create the symbolic projection functionals chi on the interferometer
        arm of direction n

        """
        # Projection functionals
        chi_plus = (source.theta_vect.T*n_vect) ** 2
        chi_plus -= (source.phi_vect.T * n_vect) ** 2
        chi_cros = 2*(source.theta_vect.T * n_vect)*(source.phi_vect.T*n_vect)

        if deriv:
            chi_plus = chi_plus.diff(self.t)
            chi_cros = chi_cros.diff(self.t)

        return chi_plus[0], chi_cros[0]

    def build_modulation_functions(self, chi_p, chi_c, psi):
        """
        Create the Sympy function for modulation factors F(theta,phi,t,psi)
        depending on source sky location, time via arm vector n(t), and wave
        polarization such that:

        DelaT(t) = L/(2c) * (h_p(t-kr/c)*F_p + h_c(t-kr/c)*F_c )


        """

        f_p = chi_p * cos(2 * psi) - chi_c * sin(2 * psi)
        f_c = chi_p * sin(2 * psi) + chi_c * cos(2 * psi)

        return f_p, f_c

    # def build_arm_response(self, source, n_vect, r_bary, armlength, ffreq=True):
    #     """
    #     Accumulated variation of light travel time along the optical pass due
    #     to the incident gravitational wave on an interferometer arm of length L
    #
    #     Parameters
    #     ----------
    #     t : sympy.Symbol instance
    #         time symbolic variable
    #     n_vect : list of sympy.matrix instances
    #         unit vectors along each arm i for i=1,2,3
    #     r_bary: sympy.matrix instance
    #         position vector of barycenter of LISA wrt the Sun
    #     armlength : sympy.Symbol instance
    #         arm length
    #     ffreq : boolean
    #         if True, the result is given in fractional frequency. Otherwise in
    #         time delay
    #     simple : boolean
    #         specify whether to apply a formal simplification of the math
    #         expressions
    #
    #     Returns
    #     -------
    #     DeltaT : sympy.core.add instance
    #         arm response to incoming GW (in accumulated time shift or
    #         fractional frequency)
    #
    #     """
    #
    #     # Modulation functions
    #     chi_plus, chi_cros = self.build_projection_functions(source, n_vect,
    #                                                          deriv=0)
    #     # # Modulation function derivatives
    #     # chi_plus_dot, chi_cros_dot = self.build_projection_functions(source,
    #     #                                                              n_vect,
    #     #                                                              deriv=1)
    #     # Taking into account polarization angle
    #     f_p, f_c = self.build_modulation_functions(chi_plus, chi_cros,
    #                                                source.psi)
    #     # f_p_dot, f_c_dot = self.build_modulation_functions(chi_plus_dot,
    #     #                                                    chi_cros_dot,
    #     #                                                    source.psi)
    #
    #     # Variation of travel time due to incoming GW
    #     # shift = (source.k_vect.T.dot(r_bary) / self.instrument.c)[0]
    #     shift = source.k_vect.T * r_bary / self.instrument.c
    #
    #     # Build h+, hx symbolic functions in source frame
    #     h_p = source.h_p(self.t)
    #     h_c = source.h_c(self.t)
    #     # Delay of light travel time due to incoming gravitational wave
    #     delta_t = h_p.subs(self.t, self.t - shift[0]) * f_p
    #     delta_t += h_c.subs(self.t, self.t - shift[0]) * f_c
    #     delta_t = armlength / (2 * self.instrument.c) * delta_t
    #
    #     # If fractional frequency requested, derive wrt time
    #     if ffreq:
    #         # Approximate time derivative
    #         # h_dot = h_p.diff(self.t).subs(self.t, self.t - shift[0]) * f_p \
    #         #         + h_c.diff(self.t).subs(self.t, self.t - shift[0]) * f_c \
    #         #         + h_p.subs(self.t, self.t - shift[0]) * f_p_dot + h_c.subs(self.t, self.t - shift[0]) * f_c_dot
    #
    #         # without any approximation , we would simply have
    #         return delta_t.diff(self.t)
    #         # return armlength / (2 * self.instrument.c) * h_dot
    #
    #     else:
    #         return delta_t

    def build_arm_response(self, source, n_vects, r_vects, i, prime=False):
        """
        Accumulated variation of light travel time along the optical pass due
        to the incident gravitational wave on an interferometer arm of length L

        Parameters
        ----------
        t : sympy.Symbol instance
            time symbolic variable
        n_vects : list of sympy.matrix instances
            unit vectors along each arm i for i=1,2,3
        r_vects: list of sympy.matrix instances
            List of LISA spacecrafts position vectors wrt the Sun
        i : int
            arm index
        prime : boolean
            if False, gives transmission in direction S/C i+1 -> i
            if True, gives direction from S/C i to i+1 (primed-indexed arm)


        Returns
        -------
        DeltaT : sympy.core.add instance
            arm response to incoming GW (in accumulated time shift or
            fractional frequency)

        """

        # Modulation functions from unit vector n_i
        chi_plus, chi_cros = self.build_projection_functions(
            source, n_vects[self.cyclic_perm(i) - 1], deriv=0)
        # Taking into account polarization angle
        f_p, f_c = self.build_modulation_functions(chi_plus, chi_cros,
                                                   source.psi)

        c_light = self.instrument.c
        if not prime:
            # Reception time
            t_re = source.k_vect.dot(
                r_vects[self.cyclic_perm(i + 1) - 1]) / c_light
            # Emission time
            t_em = (source.k_vect.dot(r_vects[self.cyclic_perm(i + 2) - 1])
                    + self.instrument.build_arm_length(
                        self.cyclic_perm(i), self.t, prime=prime)) / c_light
            denom = 2 * (1 - source.k_vect.dot(n_vects[self.cyclic_perm(i)-1]))

        else:
            # Reception time
            t_re = source.k_vect.dot(
                r_vects[self.cyclic_perm(i + 2) - 1]) / c_light
            # Emission time
            t_em = (source.k_vect.dot(r_vects[self.cyclic_perm(i + 1) - 1])
                    + self.instrument.build_arm_length(
                        self.cyclic_perm(i), self.t, prime=prime)) / c_light
            # Response denominator
            denom = 2 * (1 + source.k_vect.dot(n_vects[self.cyclic_perm(i)-1]))

        # Build h+, hx symbolic functions in source frame
        h_p = source.h_p(self.t)
        h_c = source.h_c(self.t)

        # Metric deformation at reception
        h_re = h_p.subs(self.t, self.t - t_re) * f_p
        h_re += h_c.subs(self.t, self.t - t_re) * f_c
        # Metric deformation at emission
        h_em = h_p.subs(self.t, self.t - t_em) * f_p
        h_em += h_c.subs(self.t, self.t - t_em) * f_c

        # Full response
        return (h_re - h_em) / denom

    def build_gw_signal_by_spacecraft(self, source):
        """
        Returns the gravitational signal measured by the photodetectors on
        spacecraft i, for both optical benches

        Parameters
        ----------
        t : sympy.Symbol instance
            time symbolic variable
        h_p : sympy.core.add.Add instance
            gravitational wave form + in source frame
        h_c : sympy.core.add.Add instance
            gravitational wave form x in source frame
        psi : sympy.Symbol instance
            polarization of the source
        theta_vect : sympy.matrix instance
            theta vector of the observational frame
        phi_vect : sympy.matrix instance
            phi vector of the observational frame
        k_vect : sympy.matrix instance
            unit vector along direction of wave propagation
        n_vect : list of sympy.matrix instances
            unit vectors along each arm i for i=1,2,3
        r_vect : sympy.matrix instance
            position vector of barycenter of LISA wrt the Sun
        simple : boolean
            specify whether to apply a formal simplification of the math
            expressions

        Returns
        -------
        s_GW : list of sympy.core.add instances
            signal measured onboard spacecraft i by first optical bench (i)
        s_GW_prime : list of sympy.core.add instances
            signal measured onboard spacecraft i by adjascent optical bench (i')

        Notes
        -----
        For spacecraft i = 1 :
        s_i = d/dt ( L_{i+2}/(2c) ) * [h_{s+}(t-kr/c)*F_{+}(n_{i+2})
        - h_{sx}(t-kr/c)*F_{x}(n_{i+2})]

        """

        # Construct orbit quantities
        self.orbit.build_orbital_phase(self.t)
        self.orbit.build_orbital_phase_deriv(self.t)
        self.orbit.build_barycenter_pos(self.t)

        # Compute list of unit vectors along each arm direction n_i(t)
        # (and their time derivatives)
        n_vects = [self.build_arm_vect(i, deriv=0) for i in range(1, 4)]
        # Compute list of spacecraft position vectors
        r_i_vects = [self.orbit.build_spacecraft_pos(i) for i in range(1, 4)]

        # self.s_gw_func = [self.build_arm_response(
        #     source,
        #     n_vect[self.cyclic_perm(i + 2) - 1],
        #     self.orbit.r_0_func,
        #     self.instrument.build_arm_length(i + 2, self.t, prime=False),
        #     ffreq=ffreq) for i in range(1, 4)]
        #
        # self.s_gw_prime_func = [self.build_arm_response(
        #     source,
        #     n_vect[self.cyclic_perm(i + 1) - 1],
        #     self.orbit.r_0_func,
        #     self.instrument.build_arm_length(i + 1, self.t, prime=True),
        #     ffreq=ffreq) for i in range(1, 4)]
        # Generate GW signal for obtical benches s_i, for i = {1, 2, 3}
        # corresponds to arms 3, 1, 2
        self.s_gw_func = [self.build_arm_response(
            source, n_vects, r_i_vects, self.cyclic_perm(i + 2),
            prime=False) for i in range(1, 4)]
        self.s_gw_prime_func = [self.build_arm_response(
            source, n_vects, r_i_vects, self.cyclic_perm(i + 1),
            prime=True) for i in range(1, 4)]

        # Convert symbolic functions to python callable functions
        self.s_gw_call = [sympy2func(s, usedlib="numpy")
                          for s in self.s_gw_func]
        self.s_gw_prime_call = [sympy2func(s, usedlib="numpy")
                                for s in self.s_gw_prime_func]

    # def compute_response(self, source, quantity='time_delay'):
    #
    #     # Compute list of phasemeter signals by spacecraft
    #     s_gw = self.gw_signal_by_spacecraft(source, prime=False)
    #     s_gw_prime = self.gw_signal_by_spacecraft(source, prime=True)

    def built_tdi_func(self, source):

        self.build_gw_signal_by_spacecraft(source)

        # # # List of armlength functions (numpy functions)
        # l_func = [lambda t: self.instrument.compute_arm_length(i, t,
        #                                                        prime=False)
        #           for i in range(1,4)]
        # l_prime_func = [lambda t: self.instrument.compute_arm_length(i,
        #                                                              t,
        #                                                              prime=True)
        #                 for i in range(1, 4)]
        l_func = [self.instrument.build_arm_length(i, self.t, prime=False)
                  for i in range(1, 4)]
        l_prime_func = [self.instrument.build_arm_length(i, self.t, prime=True)
                        for i in range(1, 4)]

        # Build symbolic tdi functions
        self.tdi_func = self.tdi.build_tdi_response(self.t,
                                                    self.s_gw_func,
                                                    self.s_gw_prime_func,
                                                    l_func,
                                                    l_prime_func)

        # if quantity == 'frac_freq':
        #     self.tdi_func = self.tdi_func.diff(self.t)

        # Convert it to a python callable function
        self.tdi_call = [sympy2func(tdi_func, usedlib="numpy")
                         for tdi_func in self.tdi_func]

    def compute_arm_response(self, param_dic):
        if self.s_gw_call is None:
            raise ValueError("You have to build the tdi function first")

        s_gw_mat = np.array([s_func(param_dic) for s_func in self.s_gw_call])
        s_gw_prime_mat = np.array([s_func(param_dic)
                                   for s_func in self.s_gw_prime_call])
        return s_gw_mat.T, s_gw_prime_mat.T

    def compute_tdi_response(self, param_dic):

        if self.tdi_call is None:
            raise ValueError("You have to build the tdi function first")

        tdi_list = [tdi_func(param_dic) for tdi_func in self.tdi_call]

        return tdi_list[0], tdi_list[1], tdi_list[2]

        # if quantity == 'time_delay':
        #     return self.tdi_func(self.t)
        #
        # elif quantity == 'frac_freq':
        #
        #     return derivative(self.tdi_func, self.t)
        #
        # else:
        #     raise ValueError("Quantity must be 'time_delay' or 'frac_freq'.")
