import numpy as np
from scipy.signal import butter, filtfilt, firwin, lfilter
import h5py
import configparser


def butter_bandpass(lowcut, highcut, fs, order=5):
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    b, a = butter(order, [low, high], btype='band')
    return b, a


def butter_lowpass(cutoff, fs, order=5):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    return b, a


def butter_lowpass_filter(data, cutoff, fs, order=5):

    b, a = butter_lowpass(cutoff, fs, order=order)
    # y = lfilter(b, a, data)
    y = filtfilt(b, a, data)
    return y


def design_fir_filter(cutoff, fs, order=253, beta=8.6):

    h = firwin(order + 1, cutoff, width=None,
               window=('kaiser', beta), pass_zero=True,
               scale=True, nyq=None, fs=fs)

    return h


def fir_lowpass_filter(data, cutoff, fs, order=253, beta=8.6):

    h = design_fir_filter(cutoff, fs, order=order, beta=beta)

    filtered_data = lfilter(h, 1.0, data)

    return filtered_data


class LagrangeInterpolator(object):

    def __init__(self, t, y, nint, dtype=np.float64):

        self.t = t
        self.y = y
        self.nint = nint
        self.mat = self.prod_matrix()
        self.prods = np.prod(self.mat, axis=1)

        # Sampling time
        self.ts = t[1] - t[0]
        self.nc = np.int((nint - 1) / 2)

    def prod_matrix(self):
        """

        Parameters
        ----------
        t : array_like
            time vector
        nint : scalar integer
            order of the lagrange polynomial

        Returns
        -------
        mat : 2d numpy array
            nint x nint-1 matrix

        """

        xn = self.t[0:self.nint]

        mat = np.array([xn[k]-xn[xn != xn[k]] for k in range(self.nint)],
                       dtype=self.y.dtype)

        return mat

    def single_interp(self, xmes, ymes, x):

        # The product (x - x0) (x - x1) ... (x - xn)
        full_prod = np.prod(x - xmes)
        # A vector containing all the numerators, i.e.: Prod_k k!n (x - xk)
        # for n=0...N
        nums = full_prod / (x - xmes)
        # The interpolated data
        return np.sum(nums * ymes / self.prods)

    def interp(self, t_interp):
        """

        Parameters
        ----------
        t_interp : float
            time where to interpolate the data. It is assumed that all times in
            t_interp are included in the
            time vector self.t

        Returns
        -------

        """

        # Interpolation is only valid between time t = (nint-1)/2 and times
        # t = N - 1 - (nint-1)/2
        print("value of nc " + str(self.nc))
        print("Length of t_interp " + str(t_interp.shape[0]))
        print("Length of t " + str(self.t.shape[0]))
        print("Length of y " + str(self.y.shape[0]))

        x = np.array([self.single_interp(
            self.t[np.int(t_interp[n]/self.ts)
                   - self.nc:np.int(t_interp[n]/self.ts) + self.nc + 1],
            self.y[np.int(t_interp[n]/self.ts)
                   - self.nc:np.int(t_interp[n]/self.ts) + self.nc + 1],
            t_interp[n]) for n in range(t_interp.shape[0])],
                     dtype=self.y.dtype)

        return x


# class FarrowInterpolator(object):
#
#     def __init__(self, t, y, nint):
#
#         self.__init__(self, t, y, nint)
#
#         self.t = t
#         self.y = y
#         self.nint = nint
#         self.c_m_list = []
#         self.c_m_arr = np.array(self.c_m_list)
#
#     def fdelaylti(self, N, n, d, x):
#         = delay(n,id,x) <: seq(i,N,section(i)) : !,_
#         for i in range()
#             o = (N-1.00001)/2 #; // ~center FIR interpolator
#             dmo = d - o; # // assumed >=0 [d > (N-1)/2]
#             id = np.int(dmo)
#             fd = o + (dmo - id)
#             section(i,x,y) = (x-x’) * c(i) <: _,+(y);
#             c(i) = (i - fd)/(i+1)


    # def apply_delay(d):
    #     """
    #
    #     Parameters
    #     ----------
    #     d : ndarray
    #         variable delay time series, same size as y
    #
    #     """
    #
    # def add_coeff(self, d, m):
    #
    #     self.c_m_list.append(self.c_m_list[m-1] * (d - m + 1) / m)
    #
    # def single_delay(self, i):
    #
    #     # FIR Filter coefficients
    #     filtered_data = self.lfilter(self.c_m_arr[:, i], 1.0,
    #                                  data[i - 1 - self.nint:i])
    #
    #     return filtered_data
    #
    #
    # def precompute_farrow(self, n, d, dtype=np.float64):
    #     """
    #     Parameters
    #     ----------
    #     d : ndarray
    #         variable delay time series, same size as y
    #
    #     Returns
    #     -------
    #     cm : ndarray
    #         matrix of coefficients, size (nint + 1) x n
    #
    #     """
    #
    #     # for m in range(1, n+1):
    #     #     c_m[m] = c_m[m-1] * (d0 - m + 1) / m
    #     # Compute c_m_list of size nint + 1
    #     self.c_m_list = [np.ones(d.shape[0])]
    #     _ = [self.add_coeff(m) for m in range(1, self.nint + 1)]
    #     self.c_m_arr = np.array(self.c_m_list)


def convert_lisanode_outputs(input_file_path, output_file_path):
    """Short summary.

    Parameters
    ----------
    input_file_path : str
        Path of the LISANode simulation files
    output_file_path : str
        Path where to save the hdf5 conversion

    Returns
    -------
    Nothing

    """

    # s1 = np.loadtxt(input_file_path + "s1.txt", dtype=np.float128)
    # s2 = np.loadtxt(input_file_path + "s2.txt", dtype=np.float128)
    # s3 = np.loadtxt(input_file_path + "s3.txt", dtype=np.float128)
    # s1_p = np.loadtxt(input_file_path + "s1_p.txt", dtype=np.float128)
    # s2_p = np.loadtxt(input_file_path + "s2_p.txt", dtype=np.float128)
    # s3_p = np.loadtxt(input_file_path + "s3_p.txt", dtype=np.float128)
    #
    # tau1 = np.loadtxt(input_file_path + "tau1.txt", dtype=np.float128)
    # tau2 = np.loadtxt(input_file_path + "tau2.txt", dtype=np.float128)
    # tau3 = np.loadtxt(input_file_path + "tau3.txt", dtype=np.float128)
    # tau1_p = np.loadtxt(input_file_path + "tau1_p.txt", dtype=np.float128)
    # tau2_p = np.loadtxt(input_file_path + "tau2_p.txt", dtype=np.float128)
    # tau3_p = np.loadtxt(input_file_path + "tau3_p.txt", dtype=np.float128)
    #
    # e1 = np.loadtxt(input_file_path + "epsilon1.txt", dtype=np.float128)
    # e2 = np.loadtxt(input_file_path + "epsilon2.txt", dtype=np.float128)
    # e3 = np.loadtxt(input_file_path + "epsilon3.txt", dtype=np.float128)
    # e1_p = np.loadtxt(input_file_path + "epsilon1_p.txt", dtype=np.float128)
    # e2_p = np.loadtxt(input_file_path + "epsilon2_p.txt", dtype=np.float128)
    # e3_p = np.loadtxt(input_file_path + "epsilon3_p.txt", dtype=np.float128)

    eta1 = np.loadtxt(input_file_path + "Q1.txt", dtype=np.float128)
    eta2 = np.loadtxt(input_file_path + "Q2.txt", dtype=np.float128)
    eta3 = np.loadtxt(input_file_path + "Q3.txt", dtype=np.float128)
    eta1_p = np.loadtxt(input_file_path + "Q1_p.txt", dtype=np.float128)
    eta2_p = np.loadtxt(input_file_path + "Q2_p.txt", dtype=np.float128)
    eta3_p = np.loadtxt(input_file_path + "Q3_p.txt", dtype=np.float128)

    # pm = np.column_stack([s1[:, 1], s2[:, 1], s3[:, 1]])
    # pm_prime = np.column_stack([s1_p[:, 1], s2_p[:, 1], s3_p[:, 1]])
    #
    # tau = np.column_stack([tau1[:, 1], tau2[:, 1], tau3[:, 1]])
    # tau_prime = np.column_stack([tau1_p[:, 1], tau2_p[:, 1], tau3_p[:, 1]])
    #
    # epsilon = np.column_stack([e1[:, 1], e2[:, 1], e3[:, 1]])
    # epsilon_prime = np.column_stack([e1_p[:, 1], e2_p[:, 1], e3_p[:, 1]])

    eta = np.column_stack([eta1[:, 1], eta2[:, 1], eta3[:, 1]])
    eta_prime = np.column_stack([eta1_p[:, 1], eta2_p[:, 1], eta3_p[:, 1]])

    fh5 = h5py.File(output_file_path, 'a')
    fh5.create_group("time")
    # fh5.create_dataset("time/t", data=s1[:, 0])
    fh5.create_dataset("time/t", data=eta1[:, 0])
    # Stochastic phasemeter noises
    fh5.create_group("noise")
    # fh5.create_dataset("noise/pm", data=pm)
    # fh5.create_dataset("noise/pm_prime", data=pm_prime)
    # fh5.create_dataset("noise/tau", data=tau)
    # fh5.create_dataset("noise/tau_prime", data=tau_prime)
    # fh5.create_dataset("noise/epsilon", data=epsilon)
    # fh5.create_dataset("noise/epsilon_prime", data=epsilon_prime)
    fh5.create_dataset("noise/eta", data=eta)
    fh5.create_dataset("noise/eta_prime", data=eta_prime)
    fh5.close()


def load_configuration(config_path):

    # --------------------------------------------------------------------------
    # Set simulation parameters
    # --------------------------------------------------------------------------
    config = configparser.ConfigParser()
    config.read("../configs/config_equal_arms.ini")
    # Output sampling frequency
    fs = config["Parameters"].getfloat("OutputSamplingFrequency")
    # Simulation sampling frequency
    fs_simu = config["Parameters"].getfloat("SimuSamplingFrequency")
    # Number of points
    n_points = config["Parameters"].getint("NumberOfPoints")
    # Obsevation time
    tobs = n_points / fs
    # Order of Lagrange polynomials
    nint = config["Parameters"].getint("LagrangeOrder")
    # Cutting frequency of anti-aliasing filters
    f_cut = config["Parameters"].getfloat("AntiAliasingCuttingFrequency")
    # Order of anti-alisasing filters
    order = config["Parameters"].getint("AntiAliasingOrder")
    # Re-scaling factors for non-laser noises
    # noise_fact = np.array([1, 4, 0.5, 3, 100, 2])
    noise_fact = np.array([config["Parameters"].getfloat("NoiseFactor"+str(i))
                           for i in range(1, 7)], dtype=np.float128)
    # Whether the arms are equals (and constants)
    equal_arms = config["Parameters"].getboolean("EqualArms")
    # Whether the armlenghts are constant in time
    constant_arms = config["Parameters"].getboolean("ConstantArms")
    # Whether to add Sagnac effect
    sagnac = config["Parameters"].getboolean("RotatingLisa")
    # Initial orbital angle of the constellation
    phi_rot = config["Parameters"].getfloat("InitialOrbitalAngle")
    source_config_path = config["Input"]["SourceConfigPath"]
    # Change the parameters according to the config file
    conf = loadsourceconfig.loadconfig(source_config_path)
    # Get source parameters
    param_dic = gwsources.ldc2dic(conf.p)

    return fs, fs_simu, tobs, params_dic
