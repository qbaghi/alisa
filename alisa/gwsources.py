import numpy as np
# from numpy import sin, cos, pi
from sympy import Symbol, sin, cos, Matrix, pi, tanh, Max, log  # , Heaviside
# import Cosmology


def heaviside(x):

    return Max(0, x).diff(x)


class LISASource(object):

    def __init__(self, name, theta=Symbol('theta'), phi=Symbol('phi')):

        self.name = name
        self.theta = theta
        self.phi = phi

        # Astronomical units
        # Parsec [m] (XXIX General Assembly of the International Astronomical
        # Union, RESOLUTION B2 on recommended zero points for the absolute and
        # apparent bolometric magnitude scales, 2015)
        self.pc = 3.08567758149136727e+16
        self.megapc = 1e6 * self.pc
        # GMsun in SI
        # (http://asa.usno.navy.mil/static/files/2016/Astronomical_Constants_2016.pdf)
        self.g = 6.67408e-11
        self.gmsun = 1.32712442099e+20
        self.m_sun_kg = self.gmsun/self.g
        self.c = 299792458.0

        self.theta_vect, self.phi_vect, self.k_vect = self.obs_frame_vect()

    def compute_gw_phase(self, t):

        pass

    def compute_gw_amplitude(self, t):

        pass

    def obs_frame_vect(self):
        """
        Create expression functions for observational frame vectors

        Parameters
        ----------
        theta : sympy.Symbol instance
            colatitude
        phi : sympy.Symbol instance
            longitude

        Returns
        -------
        theta_vect : sympy.matrix instance
            theta vector of the observational frame
        phi_vect : sympy.matrix instance
            phi vector of the observational frame
        k_vect : sympy.matrix instance
            unit vector along direction of wave propagation

        """

        # Observational frame (associated to the direction of propagation k)

        theta_vect = Matrix([cos(self.theta) * cos(self.phi),
                             cos(self.theta) * sin(self.phi),
                             -sin(self.theta)])
        phi_vect = Matrix([-sin(self.phi),
                           cos(self.phi),
                           0])
        k_vect = Matrix([-sin(self.theta) * cos(self.phi),
                         -sin(self.theta) * sin(self.phi),
                         -cos(self.theta)])

        return theta_vect, phi_vect, k_vect


class UCBSource(LISASource):

    def __init__(self,
                 theta=Symbol('theta'),
                 phi=Symbol('phi'),
                 f0=Symbol('f_0'),
                 fdot=Symbol('f_dot'),
                 A0=Symbol('A'),
                 phi0=Symbol('phi_0'),
                 psi=Symbol('psi'),
                 incl=Symbol('incl'),
                 name='UCB'):
        """
        Parameters
        ----------

        f_0 : sympy.Symbol instance
            GW start frequency
        f_dot : sympy.Symbol instance
            GW frequency derivative
        A0 : sympy.Symbol instance
            GW amplitude
        phi0: sympy.Symbol instance
            initial phase
        psi: sympy.Symbol instance
            polarization angle

        """

        super().__init__(name, theta, phi)

        # Symbolic variables
        self.f0 = f0
        self.fdot = fdot
        self.A0 = A0
        self.phi0 = phi0
        self.psi = psi
        self.incl = incl

    def compute_gw_phase(self, t, deriv=0):
        """
        Function directly giving the phase of the gravitational wave of a
        chirping galactic binary as a function of time

        Parameters
        ----------
        t : array_like
            time vector
        deriv : scalar integer
            integer among {0,1,2} : derivative order

        """

        if deriv == 0:

            return 2 * pi * self.f0 * t + np.pi * self.fdot * t ** 2

        elif deriv == 1:

            return 2 * pi * (self.f0 + self.fdot * t)

        elif deriv == 2:

            return 2 * pi * self.fdot

        elif deriv > 2:

            return 0

    def h_p(self, t):
        """

        h+ strain function (in the source frame) vs time

        :param self:
        :param t:
        :return:
        """

        hp = self.A0 * (1 + cos(self.incl) ** 2) * cos(self.compute_gw_phase(t)
                                                       + self.phi0)

        return hp

    def h_c(self, t):

        hc = - 2 * self.A0 * cos(self.incl) * sin(self.compute_gw_phase(t)
                                                  + self.phi0)

        return hc


class MBHBSource(LISASource):

    def __init__(self, theta, phi, incl, psi, m1s, m2s, dl, tc, phic,
                 name='MBHB'):
        """

        Parameters
        ----------

        theta : scalar float
            colatitude
        phi : scalar float
            longitude
        incl : scalar float
            inclination of the system wrt line of sight
        psi : scalar float
            polarization angle value
        m1s: scalar float
            mass of object 1 in the source frame [solar mass]
        m2s: scalar float
            mass of object 2 in the source frame [solar mass]
        dl : scalar float
            luminosity distance in MPC
        phic : scalar float
            phase at coalescence


        """

        super().__init__(name, theta, phi)

        self.incl = incl
        self.tc = tc
        self.z = Cosmology.zofDl(dl, w=0, tolerance=1e-4)[0]
        self.phic = phic
        self.psi = psi

        self.m1 = m1s * (1 + self.z) * self.m_sun_kg
        self.m2 = m2s * (1 + self.z) * self.m_sun_kg
        self.dl = dl

        # Chirp mass
        self.mc = (self.m1 * self.m2)**(3/5.) / (self.m1 + self.m2)**(1/5.)
        # Total mass
        self.mtot = self.m1 + self.m2
        # Total mass with time dimension [s]
        self.mtot_t = self.mtot * self.g / self.c**3
        # Reduced particle mass
        self.mu = (self.m1 * self.m2) / self.mtot
        # Reduced mass ratio
        self.eta = self.mu / self.mtot

        # Euler constant
        self.C_euler = 0.5772156649

        # Constants involved in phase
        self.a1n0 = 3715/8064
        self.a3n0 = 9275495/14450688
        self.a4n0 = - np.pi * 38645 / 172032
        self.a7n0 = np.pi * 188516689 / 173408256

        self.a1n1 = 55/96
        self.a3n1 = 284875/258048
        self.a4n1 = 65 * np.pi / 2048
        self.a6n1 = - 126510089885/4161798144 + np.pi**2 * 2255 / 2048
        self.a7n1 = 488825 * np.pi / 516096

        self.a3n2 = 1855 / 2048
        self.a6n2 = 154565 / 1835008
        self.a7n2 = 141769 * np.pi / 516096

        self.a6n3 = - 1179625 / 1769472

        self.a1 = self.a1n0 + self.a1n1 * self.eta
        self.a2 = - 3 * np.pi / 4
        self.a3 = self.a3n1 + self.a3n1 * self.eta + self.a3n2 * self.eta**2
        self.a4 = self.a4n0 + self.a1n1 * self.eta
        self.a5 = 831032450749357/57682522275840 - 53/40 * np.pi**2 - 107 * self.C_euler / 56 + 107/448
        self.a6 = self.a6n1 * self.eta + self.a6n2 * self.eta**2 + self.a6n3 * self.eta**3
        self.a7 = self.a7n0 + self.a7n1 * self.eta + self.a7n2 * self.eta**2

        # Constants involved in frequency
        self.b2n0 = 743/2688
        self.b4n0 = 1855099/14450688
        self.b2n1 = 11/32
        self.b4n1 = 56975/258048
        self.b4n2 = 371 / 2048

        self.b2 = self.b2n0 + self.b2n1 * self.eta
        self.b3 = -3*np.pi/10
        self.b4 = self.b4n0 + self.b4n1 * self.eta + self.b4n2 * self.eta**2

        # Initial reduced time
        self.tau0 = self.eta * self.tc / (5 * self.mtot_t)
        # Initial angular frequency
        self.omega0 = self.build_omega(self.tau0)

    def build_rescaled_time(self, t):
        """

        :param t: sympy.Symbol instance
            time vector [s]
        :param dt: scalar
            short time before merger time where we set the rescaled time to a constant value equal to
            tau(tc - dt) to prevent any infinite number from arising.
        :return tau: numpy array
            rescaled time [no dimension]
        """

        # tau = np.zeros(t.shape[0])
        # tau[t < self.tc - dt] = self.eta * (self.tc - t[t < self.tc - dt]) / (5 * self.mtot_t)

        return self.eta * (self.tc - t) / (5 * self.mtot_t)

    def build_omega(self, tau):
        """

        :param tau: sympy.matrix instance
            rescaled time [no dimension]
        :return: m_omega : sympy.matrix instance
            GW angular frequency [s-1]
        """

        # omega = (1 / (8 * self.mtot_t)) * tau**(-3/8.) * (1 + (11/32*self.eta + 743/2688) * tau**(-1/4.)
        #                                                   - 3/10 * np.pi * tau**(-3/8.)
        #                                                   + (1855099/14450688 + 56975/258048 * self.eta
        #                                                      + 371/2048 * self.eta**2) * tau**(-1/2))
        omega = (1 / (8 * self.mtot_t)) * (tau**(-3/8) + self.b2 * tau**(-5/8) + self.b3 * tau**(-1/2)
                                           + self.b4 * tau**(-7/8))

        return omega

    # def build_phase(self, momeg):
    #
    #     phase = self.phic - 1 / (32*self.eta) * momeg**(-5/3) \
    #             * (1 + (3715/1008 + 55/12*self.eta)*momeg**(2/3) - 10*np.pi*momeg
    #                + (15293365/1016064 + 27145/1008 * self.eta + 3085/144 * self.eta**2) * momeg**(4/3))
    #
    #     return phase

    def build_phase(self, tau, omega):
        """

        :param tau: sympy.matrix instance
            rescaled time [no dimension]
        :return: phase : sympy.matrix instance
            GW phase [rad]
        """

        phi = - (1/self.eta) * (tau**(5/8) + self.a1 * tau**(3/8) + self.a2 * tau**(1/4)
                                + self.a4 * log(tau/self.tau0) + self.a5 * log(tau / 256) + self.a6 +
                                self.a7 * tau**(-1/4))

        phase = phi - 2 * self.mtot_t * omega * log(omega/self.omega0)

        # phase = self.phic - tau**(5/8) / self.eta * (1 + (3715/8064 + 55/96 * self.eta) * tau**(-1/4) - 3*4*np.pi/16
        #                                              + (9275495/14450688 + 284875/258048 * self.eta
        #                                                 + 1855/2048 * self.eta**2) * tau**(-1/2))

        return phase

    def taper_func(self, momeg, a=150):

        return 1/2 * (1 - tanh(a * (momeg * self.g / self.c**3)**(2/3) - 1/7))

    def h_p(self, t):
        """

        h+ strain function (in the source frame) vs time

        :param self:
        :param t:
        :return:
        """

        # Adimensional rescaled times
        tau = self.build_rescaled_time(t)
        # M * omega(tau) in kg.s-1
        # momeg = self.build_m_omega(tau)
        # Angular frequency in .s-1
        omeg = self.build_omega(tau)
        # Adimensional phase
        phase = self.build_phase(tau, omeg)
        # Adimensional strain (+ polarization)
        amp = - 2 * self.g * self.mtot * self.eta / (self.c**2 * self.dl * self.megapc) * (self.mtot_t * omeg)**(2/3)
        hp = amp * (1 + np.cos(self.incl)**2) * cos(2 * phase)
        # hp = 2 * self.g * self.mu / (self.dl * self.megapc * self.c**2) \
        #      * (momeg * self.g / self.c**3)**(2/3) * (1 + np.cos(self.incl)**2) * cos(2 * phase)

        return hp # * heaviside(t - self.tc)# self.taper_func(momeg, a=150) * hp

    def h_c(self, t):

        # Adimensional rescaled times
        tau = self.build_rescaled_time(t)
        # M * omega(tau) in kg.s-1
        # momeg = self.build_m_omega(tau)
        # Angular frequency in .s-1
        omeg = self.build_omega(tau)
        # Adimensional phase
        phase = self.build_phase(tau, omeg)
        # Adimensional strain (+ polarization)
        amp = - 2 * self.g * self.mtot * self.eta / (self.c**2 * self.dl * self.megapc) * (self.mtot_t * omeg)**(2/3)
        hc = amp * 2 * np.cos(self.incl) * sin(2 * phase)

        return hc

    def h_pc(self, t):

        # Adimensional rescaled times
        tau = self.build_rescaled_time(t)
        # M * omega(tau) in kg.s-1
        # momeg = self.build_m_omega(tau)
        # Angular frequency in .s-1
        omeg = self.build_omega(tau)
        # Adimensional phase
        phase = self.build_phase(tau, omeg)
        # Adimensional strain (+ polarization)
        amp = - 2 * self.g * self.mtot * self.eta / (self.c**2 * self.dl * self.megapc) * (self.mtot_t * omeg)**(2/3)
        hp = amp * (1 + np.cos(self.incl)**2) * cos(2 * phase)
        hc = amp * 2 * np.cos(self.incl) * sin(2 * phase)

        return hp, hc


def ldc2dic(p):
    """
    Function that convert the value of a LDC parameter instance as described
    in LDC waveforms into a dictionary of parameters in the Analyitic LISA
    nomenclatura
    """
    pdic = p.__dict__['pars']

    param_dic = {"theta": pdic["EclipticLatitude"] + np.pi/2,
                 "phi": pdic["EclipticLongitude"] - np.pi,
                 "f_0": pdic["Frequency"],
                 "f_dot": pdic["FrequencyDerivative"],
                 "A": pdic["Amplitude"],
                 "psi": pdic["Polarization"],
                 "incl": pdic["Inclination"],
                 "phi_0": pdic["InitialPhase"],
                 "Tobs": pdic["ObservationDuration"],
                 "fs": 1/pdic["Cadence"]}

    # # Specify time vector
    # n_data = np.int(pdic["ObservationDuration"]/pdic["Cadence"])
    # t = np.arange(0, n_data)*pdic["Cadence"]
    # param_dic.update({"t":t})

    return param_dic
