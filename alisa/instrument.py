import numpy as np
from alisa import lisanoise


l_vect = np.array([2.5e9 for i in range(6)])


def sagnac_delay(delay_0, t_orb, c=299792458.0):

    # Orbital angular frequency (rad s-1)
    omega = 2 * np.pi / t_orb
    # equivalent aeras [s^2]
    areas = np.sqrt(3) * delay_0 ** 2 / 4
    # sagnac effect [m]
    dl = 4 * omega * areas / c  # s-1 * s2 / (ms-1) = s2/m

    return dl


class LISAInstrument(object):
    """
    Features of the simulation and the instrument
    """

    def __init__(self, tobs, fs=1.0, fs_simu=10.0, arms=l_vect):
        """

        Parameters
        ----------
        tobs : float
            observation time [s]
        fs : float
            output sampling frequency [Hz]
        fs_simu : float
            internal simulation sampling frequency [Hz]
        arms : list of callables
            list of function giving armlengths as a function of time [m]
        """

        self.Tobs = tobs
        self.fs = fs
        self.c = 299792458.0
        self.T = 365.25 * 24. * 3600
        self.arms = arms
        self.noise = lisanoise.NoiseModel(fs, fs_simu=fs_simu)

    def compute_time_vector(self):

        return np.arange(0, np.int(self.Tobs*self.fs))/self.fs

    def build_arm_length(self, i, t, prime=False):
        """

        Parameters
        ----------
        i : int
            arm index
        t : Sympy.Symbol object or ndarray
            time
        prime : bool
            If False, returns Li. If True, returns Li'. Default is False.

        Returns
        -------

        """
        if not prime:
            return self.arms[i - 1](t)
        else:
            return self.arms[i - 1 + 3](t)
