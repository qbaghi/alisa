# -*- coding: utf-8 -*-
# Author: Quentin Baghi 2019

import numpy as np
from sympy import pi, Matrix, cos, sin


class LISAorbit(object):
    """
    Object defining an analytical orbit for LISA

    """

    def __init__(self, phi_rot=0,
                 mean_arm_length=2.5e9,
                 e=9.6e-3):

        # LISA orbital period in seconds
        self.T = 365.25 * 24. * 3600
        self.phi_T = None  # self.compute_orbital_phase(t)
        self.phi_T_dot = None  # self.compute_orbital_phase_deriv(t)
        self.phi_rot = phi_rot
        self.omega = 2*np.pi/self.T
        self.L = mean_arm_length
        self.c = 299792458.0
        # Distance from lisa barycenter to SSB
        self.R = 149597870700.
        self.r_0 = None
        self.r_0_func = None
        # Eccentricity
        self.e = e

    def build_orbital_phase(self, t):

        self.phi_T = 2*pi*t/self.T

    def build_orbital_phase_deriv(self, t):

        self.phi_T_dot = 2*pi/self.T

    def build_barycenter_pos(self, t):

        if self.phi_T is None:
            self.build_orbital_phase(t)

        r_0 = Matrix([self.R * cos(self.phi_T),
                      self.R * sin(self.phi_T),
                      0])

        self.r_0_func = r_0

    def build_spacecraft_pos(self, i):
        """
        Position of S/C i
        """

        phi_rot_i = self.phi_rot_i_func(i)

        # Position of S/C with respect to LISA's barycenter
        dr_i = self.R * self.e / 2 * Matrix(
            [cos(2*self.phi_T - phi_rot_i) - 3 * cos(phi_rot_i),
             sin(2*self.phi_T - phi_rot_i) - 3 * sin(phi_rot_i),
             - 2 * np.sqrt(3) * cos(self.phi_T - phi_rot_i)])
        # Position of S/C with respect to SSB
        return self.r_0_func + dr_i

    def phi_rot_i_func(self, i):
        """

        Parameters
        ----------
        Phi_rot : sympy.Symbol instance
            Initial angle of constellation wrt to nominal configuration
            (see Antoine's thesis)
        i : integer
            index of S/C

        Returns
        -------
        Phi_rot_i : sympy.core.add.Add instance
            the angle of rotation of satellite i wrt the reference orbit
        """

        return (i - 1) * 2 * np.pi / 3 - self.phi_rot

    def constant_delay(self, t, i, sagnac=True):

        delay_i = self.compute_delay(0, np.abs(i))

        # If rotatingwise and rotating LISA
        if (i > 0) & (sagnac):
            areas = np.sqrt(3) * delay_i ** 2 / 4  # [s^2]
            dl = 4 * self.omega * areas  # [s^-1 * s^2 = s]
            delay_i = delay_i + dl / 2

        # If primed delay (counterrotatingwise) and rotating LISA
        elif (i < 0) & (sagnac):
            areas = np.sqrt(3) * delay_i ** 2 / 4  # [s^2]
            dl = 4 * self.omega * areas  # [s^-1 * s^2 = s]
            delay_i = delay_i - dl / 2

        tt = type(t)
        if (tt == np.float128) | (tt == np.float64) | (tt == np.int):
            return delay_i
        elif tt == np.ndarray:
            return delay_i * np.ones(t.shape[0])

    def linear_delay(self, t, i):

        delay_i = self.compute_delay(0, i, derivative=0)
        delay_i += self.compute_delay(0, i, derivative=1) * t

        return delay_i

    def compute_delay(self, t, i, derivative=0):
        """
        Compute the time delay as a function of time for the three LISA
        spacecraft on realistic trajectories, modeled as
        eccentric, inclined solar orbits


        Parameters
        ----------
        t : array_like
            time vector
        i : scalar integer
            arm number
        derivative : scalar integer
            if 0, computes the L/c(t) function. If 1, compute its first time
            derivative

        Returns
        -------
        Delta_i : numpy array
            delay time series for interspacecraft distance i-(i+1) [seconds]

        References
        ----------
        Tinto, Vallisneri and Amstrong 2018 -> equation is wrong
        Rather refers to Cornish and Rubbo, LISA Response function
        PHYSICAL REVIEW D 67, 022001 2003


        """

        # Average delay in seconds
        delta_0 = self.L/self.c

        if derivative == 0:
            delta_i = delta_0 * (1 + (self.e/32.) * np.sin(3 * self.omega * t
                                                           - 3 * self.phi_rot)
                                 - self.e * (15 / 32.) * np.sin(
                                     self.omega * t - self.phi_rot_i_func(i)))
        elif derivative == 1:
            delta_i = delta_0 * ((3 * self.omega / 32.) * self.e * np.cos(
                3 * self.omega * t - 3 * self.phi_rot)
                                 - self.e * self.omega * (15 / 32.) * np.cos(
                                     self.omega * t - self.phi_rot_i_func(i)))

        return delta_i
